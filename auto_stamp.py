import sys, re, time, json, random, glob, os, argparse, urllib, urllib.request
from subprocess import run, PIPE
from PIL import Image, ImageFont, ImageDraw, ImageEnhance, ImageOps, ImageFilter
from hashlib import md5
import dbus


HERE = os.path.dirname(os.path.abspath(__file__))


def wrap_text(text, font, maxwidth):
    #https://itnext.io/how-to-wrap-text-on-image-using-python-8f569860f89e
    ans = ['']
    for word in text.split():
        l = (ans[-1]+' '+word).strip()
        if font.getsize(l)[0] <= maxwidth:
            ans[-1] = l
        else:
            ans.append(word)
    return ans


def outlined(draw, x, y, shadow=(0,0,0,64), outline=4, **kwargs):
    #https://stackoverflow.com/a/52999347/3671939
    kwshadow = {**kwargs, 'fill':shadow}
    dxy = [(dx,dy) for dx in [-1,0,1] for dy in [-1,0,1]]
    for i in range(1, 1+outline):
        for dx,dy in dxy:
            draw.text((x+dx*i, y+dy*i), **kwshadow)
    draw.text((x, y),  **kwargs)
    return


def stamp_blocks(img, blocks, margin=0.03):
    assert margin < 0.5, 'non sense margin value'
    width = 1 - 2*margin
    
    X, Y = img.size
    max_width = round(width*X)

    blocks = [b.copy() for b in blocks]
    for b in blocks:
        assert 0 < b['size'] <= 1, 'Size too big. Units are height percentage.'
        size = round(Y*b['size'])
        b['font'] = ImageFont.truetype(font=b['font_path'], size=size)
        b['lines'] = wrap_text(b['text'], b['font'], max_width)
        b['width'] = max(b['font'].getsize(l)[0] for l in b['lines'])
        b['lheight'] = b['font'].size # line_height
        b['height'] = len(b['lines'])*b['lheight']

    margin = round(margin*X)
    y1 = Y-margin
    y0 = y1 - sum(b['height'] for b in blocks)
    x0 = margin
    
    x,y = x0, y0
    img = img.copy()
    draw = ImageDraw.Draw(img)
    for b in blocks:
        outline = max(1, round(0.04*b['font'].size))
        for line in b['lines']:
            outlined(
                draw, x, y,
                text=line,
                fill=b['fill'],
                font=b['font'],
                outline=outline,
                shadow=b['shadow'],
            )
            y += b['lheight']
    return img


def info_stamp(img, info, font_path, defaults=None, **kwargs):
    defaults = {
        'text': '',
        'size': 0.016,
        'fill': (255,255,255),
        'shadow': (0,0,0),
        'font_path': font_path,
        **(defaults or {})
    }
    blocks = [
        {
            'text': info['title'],
            'size': 0.040,
        }
    ]
    if 'description' in info:
        blocks.append({'size': 0.010})
        blocks.append({'size': 0.020, 'text':info['description']})
    if 'details' in info:
        blocks.append({'size': 0.010})
        blocks.append({'size': 0.016, 'text':info['details']})
    # Set missing values to default
    for b in blocks:
        for key in defaults:
            b[key] = b.get(key, defaults[key])
    return stamp_blocks(img, blocks, **kwargs)


def stamp_wallpaper(basepath, font_path):
    info = load_info(basepath)
    img = Image.open(f'{basepath}.jpg')
    img = info_stamp(img, info, font_path)
    return img


def load_info(basepath):
    try:
        with open(f'{basepath}.json') as f:
            info = json.loads(f.read())
    except:
        basename = basepath.split('/')[-1]
        info = {
            'filename': f'{basename}.jpg',
            'details': f'{basename}',
        }
    return info


def main(font_path=None):
    if font_path==None:
        font_path = "/usr/share/fonts/truetype/lato/Lato-Medium.ttf"
    run(['mkdir', '-p', f'{HERE}/collection'])
    run(['mkdir', '-p', f'{HERE}/stamped'])
    for x in glob.glob(f'{HERE}/collection/*.json'):
        basename = x.split('/')[-1][:-5]
        img = stamp_wallpaper(f'{HERE}/collection/{basename}', font_path)
        img.save(f'{HERE}/stamped/{basename}.jpg')
        print(basename)
    return


if __name__ == '__main__':
    prog, *args = sys.argv
    main(*args)
