import sys, re, time, json, random, glob, os, argparse, urllib, urllib.request
from subprocess import run, PIPE
from PIL import Image, ImageFont, ImageDraw, ImageEnhance, ImageOps, ImageFilter
from hashlib import md5
import dbus


HERE = os.path.dirname(os.path.abspath(__file__))


def wrap_text(text, font, maxwidth):
    #https://itnext.io/how-to-wrap-text-on-image-using-python-8f569860f89e
    ans = ['']
    for word in text.split():
        l = (ans[-1]+' '+word).strip()
        if font.getsize(l)[0] <= maxwidth:
            ans[-1] = l
        else:
            ans.append(word)
    return ans


def outlined(draw, x, y, shadow=(0,0,0,64), outline=4, **kwargs):
    #https://stackoverflow.com/a/52999347/3671939
    kwshadow = {**kwargs, 'fill':shadow}
    dxy = [(dx,dy) for dx in [-1,0,1] for dy in [-1,0,1]]
    for i in range(1, 1+outline):
        for dx,dy in dxy:
            draw.text((x+dx*i, y+dy*i), **kwshadow)
    draw.text((x, y),  **kwargs)
    return


def stamp_blocks(img, blocks, margin=0.05):
    assert margin < 0.5, 'non sense margin value'
    width = 1 - 2*margin
    
    X, Y = img.size
    max_width = round(width*X)

    blocks = [b.copy() for b in blocks]
    for b in blocks:
        assert 0 < b['size'] <= 1, 'Size too big. Units are height percentage.'
        size = round(Y*b['size'])
        b['font'] = ImageFont.truetype(font=b['font_path'], size=size)
        b['lines'] = wrap_text(b['text'], b['font'], max_width)
        b['width'] = max(b['font'].getsize(l)[0] for l in b['lines'])
        b['lheight'] = b['font'].size # line_height
        b['height'] = len(b['lines'])*b['lheight']

    y1 = round(Y*(1-margin))
    y0 = y1 - sum(b['height'] for b in blocks)
    x0 = round(margin*X)
    
    x,y = x0, y0
    img = img.copy()
    draw = ImageDraw.Draw(img)
    for b in blocks:
        outline = max(1, round(0.04*b['font'].size))
        for line in b['lines']:
            outlined(
                draw, x, y,
                text=line,
                fill=b['fill'],
                font=b['font'],
                outline=outline,
                shadow=b['shadow'],
            )
            y += b['lheight']
    return img


def info_stamp(img, info, font_path, defaults=None, **kwargs):
    defaults = {
        'text': '',
        'size': 0.016,
        'fill': (255,255,255),
        'shadow': (0,0,0),
        'font_path': font_path,
        **(defaults or {})
    }
    blocks = [
        {
            'text': info['title'],
            'size': 0.040,
        }
    ]
    if 'description' in info:
        blocks.append({'size': 0.010})
        blocks.append({'size': 0.020, 'text':info['description']})
    if 'details' in info:
        blocks.append({'size': 0.010})
        blocks.append({'size': 0.016, 'text':info['details']})
    # Set missing values to default
    for b in blocks:
        for key in defaults:
            b[key] = b.get(key, defaults[key])
    return stamp_blocks(img, blocks, **kwargs)


def get_desktop_resolution():
    info = run('xdpyinfo', stdout=PIPE).stdout.decode()
    res = re.search(r'dimensions: *(\d+)x(\d+)', info).groups()
    return tuple(int(x) for x in res)


def crop_center(img, rX, rY, fill=None):
    X, Y = img.size
    area = ((X-rX)//2, (Y-rY)//2, X-(X-rX+1)//2, Y-(Y-rY+1)//2)
    if X>=rX and Y>=rY:
        img = img.copy().crop(area)
    else:
        #https://stackoverflow.com/a/50775565/3671939
        max_val = 3 + max(area[0],area[1],X-area[2],Y-area[3])
        exp = ImageOps.expand(img.copy(), border=max_val, fill=fill)
        img = crop_center(exp, rX, rY, fill)
    return img


def scale(img, factor):
    X, Y = img.size
    fX = round(X*factor)
    fY = round(Y*factor)
    return img.resize((fX,fY))


def blurred_bg(img, rX, rY, radius=None):
    X, Y = img.size
    if radius==None: radius=0.008
    radius = round(radius*(X*Y)**.5)
    blur = ImageFilter.GaussianBlur(radius=radius)
    bg = scale(img, max(rX/X, rY/Y)).filter(blur)
    bg = crop_center(bg, rX, rY)
    return bg


def auto_crop(img, rX, rY, scale_factor=None, blur_radius=None):
    if scale_factor!=None:
        img = scale(img, scale_factor)
    X, Y = img.size
    if X<rX or Y<rY:
        img = img.convert('RGBA')
        out = crop_center(img, rX, rY, fill=(0,0,0,140))
        bg = blurred_bg(img, rX, rY, blur_radius)
        out = Image.alpha_composite(bg, out)
        out = out.convert('RGB')
    else:
        out = crop_center(img, rX, rY)
    return out


def variants(img, info, resizing, font_path):
    if resizing=='never':
        return info_stamp(img, info, font_path)
    X, Y = img.size
    rX, rY = get_desktop_resolution()
    if resizing=='match':
        return info_stamp(img.resize((rX, rY)), info, font_path)
    elif resizing=='off':
        rimg = auto_crop(img, rX, rY)
        return info_stamp(rimg, info, font_path)
    elif resizing=='fit':
        rimg = auto_crop(img, rX, rY, min(rX/X, rY/Y))
        return info_stamp(rimg, info, font_path)
    elif resizing=='cover':
        rimg = auto_crop(img, rX, rY, max(rX/X, rY/Y))
        return info_stamp(rimg, info, font_path)
    else:
        raise Exception(f'No such mode {resizing}')
    return


def save(img, info):
    basepath = '{HERE}/collection/{filename}'.format(HERE=HERE, **info)
    with open(f'{basepath}.json', 'w') as f:
        f.write(json.dumps(info, indent=4))
    img.save(f'{basepath}.jpg')
    return basepath


def make_wallpaper(basepath, resizing, font_path):
    try:
        with open(f'{basepath}.json') as f:
            info = json.loads(f.read())
    except:
        basename = basepath.split('/')[-1]
        info = {
            'filename': f'{basename}.jpg',
            'details': f'{basename}',
        }
    img = Image.open(f'{basepath}.jpg')
    img = variants(img, info, resizing, font_path)
    return img


def set_wallpaper(filepath, plugin='org.kde.image'):
    #https://github.com/pashazz/ksetwallpaper/blob/master/ksetwallpaper.py
    jscript = """
    var allDesktops = desktops();
    print (allDesktops);
    for (i=0;i<allDesktops.length;i++) {
        d = allDesktops[i];
        d.wallpaperPlugin = "%s";
        d.currentConfigGroup = Array("Wallpaper", "%s", "General");
        d.writeConfig("Image", "file://%s")
    }
    """
    bus = dbus.SessionBus()
    plasma = dbus.Interface(bus.get_object('org.kde.plasmashell', '/PlasmaShell'), dbus_interface='org.kde.PlasmaShell')
    plasma.evaluateScript(jscript % (plugin, plugin, filepath))
    return


def load_config(path):
    with open(path) as f:
        config = json.loads(f.read())
    units = {'s':1, 'm':60, 'h':3600, 'd':24*3600}
    seconds = lambda s:int(s[:-1])*units[s[-1].lower()]
    config['every'] = sum(seconds(s) for s in config['every'].split())
    #'/usr/share/fonts/truetype/ubuntu/Ubuntu-R.ttf'
    return config


def set_random_wallpaper(resizing, font_path, tmp):
    wps = glob.glob(f'{HERE}/collection/*.jpg')
    wp = random.choice(wps)
    img = make_wallpaper(wp[:-4], resizing, font_path)
    img.save(tmp)
    print(f'Setting wallpaper {wp}')
    set_wallpaper('')
    set_wallpaper(tmp)
    print('ok')
    return


def main_watchdog(cfg):
    '''
    Yield config every x time or whenever the file changes
    x is determined by the file itself: parameter 'every'
    '''
    def fsleep(e, max_time=5):
        return max(0.1, e/round(e/max_time+.6))
    e = sleep = prev_t = prev_mtime = 0
    while 1:
        t = time.time()
        mtime = os.path.getmtime(cfg)
        if mtime!=prev_mtime or t-prev_t>e:
            prev_mtime = mtime
            prev_t = t
            config = load_config(cfg)
            e = config['every']
            sleep = fsleep(e)
            yield config
        time.sleep(sleep)
    return


def get_natgeo():
    #https://github.com/gabsprates/photo-of-the-day/blob/master/getDesktopBG.sh
    url  = "http://www.nationalgeographic.com/photography/photo-of-the-day/"
    content = urllib.request.urlopen(url).read().decode('utf8')
    info = {
        'img_url': r'meta *property="og:image" *content="(.*)"',
        'title': r'meta *property="og:title" *content="(.*)"',
        'description': r'"dek":{"text":"(.*?)"', 
    }
    info = {
        key:re.findall(val, content)[0]
        for key,val in info.items()
    }
    fix = lambda s: re.sub(r'<.*?>', '', s.replace('<br>', '\n'))
    info['description'] = fix(info['description'])
    img_data = urllib.request.urlopen(info['img_url'])
    img = Image.open(img_data).convert("RGB")
    regex = r'/pod-(?P<day>\d+)-(?P<month>\d+)-(?P<year>\d+)'
    date = re.search(regex, info['img_url']).groupdict()
    date = '{year}-{month}-{day}'.format(**date)
    info['details'] = f'National Geographic, {date}'
    info['filename'] = f'natego-{date}'
    return img, info


def my_hash(s):
    h = md5(s.encode('utf8')).hexdigest()
    return f'{h[0:4]}-{h[4:8]}-{h[8:12]}'


def get_welt(url):
    #url="#https://www.welt.de/vermischtes/bilder-des-tages/gallery208856067/Fotografie-Die-besten-Bilder-des-Tages.html"
    content = urllib.request.urlopen(url).read().decode('utf8')
    all_info = []
    regex = r'data-src="(.*?)"'
    imgs = re.findall(regex, content)
    regex = r"""class="o-element__(?:caption-)?text".*?>([^<]+?)<"""
    texts = [s.strip() for s in re.findall(regex, content) if s.strip()]
    for img, text in zip(imgs, texts):
        title = text[:text.find(':')].strip()
        text = text[text.find(':')+1:].strip()
        info = {
            'img_url': img,
            'title': title,
            'description': text,
            'details': 'Welt.de',
            'filename': f'welt-{my_hash(img)}',
        }
        print(info)
        save(info)
        time.sleep(3)
    return


def save(info, collection=f''):
    img_data = urllib.request.urlopen(info['img_url'])
    img = Image.open(img_data).convert("RGB")
    with open(f'{HERE}/collection/{filename}.json', 'w') as f:
        f.write(json.dumps(info, indent=4))
    img.save(f'{HERE}/collection/{filename}.jpg')
    return f'{HERE}/collection/{filename}'


def main():
    #os.chdir(HERE)
    run(['mkdir', '-p', f'{HERE}/collection'])
    for config in main_watchdog(f'{HERE}/config.json'):
        set_random_wallpaper(
            config['resizing'],
            config['font_path'],
            f'{HERE}/wallpaper.jpg',
        )
    return


if __name__ == '__main__':
    prog, *args = sys.argv
    if not args:
        main()
    else:
        action, *args = args
        if action=='natgeo': get_natgeo(*args)
        elif action=='welt': get_welt(*args)
        else: raise Exception('parameter not understood')
