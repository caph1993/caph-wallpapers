
resizing:
 + "off": Image size is not changed.
 + "fit": Image fits inside the screen keeping proportions.
 + "cover": Image covers the screen completely keeping proportions.
 + "match": Image fits and covers your screen ignoring the original aspect ratio.

rotation:
 + 1: Use always the latest wallpaper.
 + n: Rotate the latest n wallpapers in order.
 + 0: Rotate all available wallpapers in order.
